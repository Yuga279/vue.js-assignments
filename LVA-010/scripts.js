function imgSlider() {
    new Vue({
        el: '#app',
        data: {
            imgs: ["Dog-1.jpg", "seal-2.jpg", "lighthouse-3.jpg", "chai-4.jpg", "mountain-5.jpg", "feild-6.jpg"],
            index: 1,
        },
        methods: {
            selectedImage(n) {
                this.index = n;
            },
            prev() {
                if (this.index == 1) {
                    this.index = 6;
                } else {
                    this.index = this.index - 1;
                }
            },
            nxt() {
                if (this.index == 6) {
                    this.index = 1;
                } else {
                    this.index = this.index + 1;
                }
            },
            changeImage() {
                if (this.index == 6) {
                    this.index = 1;
                } else {
                    this.index = this.index + 1;
                }
            },
            amIselected(divId) {
                return this.index == divId
            }
        },
        computed: {
            slide() {
                return this.imgs[this.index - 1];
            }
        },
        created: function() {
            var autoslide = this;
            setInterval(() => {
                autoslide.changeImage();
            }, 3000);
        }
    });
}