function LoanApplication() {

    Vue.component('simple-component', Vue.extend({
        template: '#componentforaddress',
        props: ['loanObject'],
        data: function () {
            return {
                profCity: ["Chennai", "coimbatore", "Kochi", "Madurai", "Banglore"],
                proState: ["TamilNadu", "Kerala", "Karnataka"],


                profSelectedState: '',
                profSelectedCity: '',
                emplSelectedState: '',
                emplSelectedCity: '',
                propSelectedState: '',
                propSelectedCity: '',
                propZipcode: '',
                emplZipcode: '',
                profZipcode: '',
            }
        },
    }));

    new Vue({
        el: "#app",
        data: {
            Profile: true,
            Employement: false,
            Property: false,
            preview: false,
            submit: false,
            typofproperty: ["Land loan", "Home loan", "Car loan", "Bike loan", "personal Loan"],
            selectedproperty: '',

            loanProf: {
                firstName: '',
                lastName: '',
                business: '',
                profaddress: '',
                city: '',
                state: '',
                zipCode: '',
            },
            loanEmployee: {
                NameofEmployer: '',
                yrWithEmployee: '',
                salary: '',
                empladdress: '',
                city: '',
                state: '',
                zipCode: '',
            },
            loanProperty: {
                propName: '',
                propLocation: '',
                city: '',
                state: '',
                zipCode: '',
            }
        },
        methods: {
            shemp() {
                this.Employement = true;
                this.Profile = false;
                this.Property = false;
                this.preview = false;
                this.submit = false;
            },
            shprofile() {
                this.Employement = false;
                this.Profile = true;
                this.Property = false;
                this.preview = false;
                this.submit = false;
            },
            shproperty() {
                this.Profile = false;
                this.Employement = false;
                this.Property = true;
                this.preview = false;
                this.submit = false;
            },
            shpreview() {
                this.Profile = false;
                this.Employement = false;
                this.Property = false;
                this.preview = true;
                this.submit = false;
            },
            submitview() {
                this.Profile = false;
                this.Employement = false;
                this.Property = false;
                this.preview = false;
                this.submit = true;
            },
            clearcontent() {
                this.Profile = true;
                this.Employement = false;
                this.Property = false;
                this.preview = false;
                this.submit = false;

                this.loanProf.firstName = " ";
                this.loanProf.lastName = " ";
                this.loanProf.business = " ";
                this.loanProf.profaddress = " ";

                this.loanEmployee.NameofEmployer = "";
                this.loanEmployee.yrWithEmployee = "";
                this.loanEmployee.salary = "";


                this.loanProperty.propName = "";
                this.loanProperty.propLocation = "";
                this.selectedproperty = " ";

            }
        },

    }
    )
}
