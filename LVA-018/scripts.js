function axisBank() {
    Vue.component('axixBank-card-component', Vue.extend({
        template: '#welcomeToAxixbank',
        methods: {
            showEnquiry() {
                this.$emit('index', 'detailed-enquiry-component')
            }
        }
    }));
    Vue.component('detailed-enquiry-component', Vue.extend({
        template: '#enqueryOptions',
        methods: {
            showAccType() {
                this.$emit('index', 'account-type-component')
            }
        }
    }));
    Vue.component('account-type-component', Vue.extend({
        template: '#accountType',
        methods: {
            showAmt() {
                this.$emit('index', 'enter-amount-component')
            }
        }
    }));
    Vue.component('enter-amount-component', Vue.extend({
        template: '#enterAmount',
        methods: {
            showPin() {
                this.$emit('index', 'enter-Yourpin-component')
            }
        }
    }));
    Vue.component('enter-Yourpin-component', Vue.extend({
        template: '#enterYourPin',
        data: function () {
            return {
                userpin: 7224,
                inputpin: '',
            }
        },
        methods: {
            shownxt() {
                if (this.userpin == this.inputpin) {
                    this.$emit('index', 'dispensingCash-component');
                } else {
                    this.$emit('index', 'incorrect-pin-component');
                }
            }
        }
    }));
    Vue.component('incorrect-pin-component', Vue.extend({
        template: '#inCorrectPin',
    }));
    Vue.component('dispensingCash-component', Vue.extend({
        template: '#dispensingCash',
        mounted() {
            setInterval(this.viewMore, 3000)
        },
        methods: {
            viewMore() {
                this.$emit('index', 'success-Message-component');
            }
        }
    }));
    Vue.component('success-Message-component', Vue.extend({
        template: '#successMessage',
    }));
    new Vue({
        el: "#app",
        data: {
            atmprocess: 'axixBank-card-component'
        },
        methods: {
            countcomponent(index) {
                this.atmprocess = index;
            }
        }
    })
}






