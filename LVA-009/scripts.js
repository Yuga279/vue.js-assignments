function order() {
    Vue.component(
        "ordered-component",
        Vue.extend({
            template: "#orderListcomponent",
            props: ["order"],
            data: function () {
                return {
                    count: false,
                    add: true,
                };
            },
            methods: {
                addbtn() {
                    if (this.order.increment < 99) {
                        this.order.increment++;
                    }
                    (this.add = false), (this.count = true);
                },
                incrementValue() {
                    if (this.order.increment < 99) {
                        this.order.increment++;
                    }
                    //    console.log(x);
                },
                decrementValue() {
                    if (this.order.increment > 0) {
                        this.order.increment--;
                        if (this.order.increment == 0) {
                            (this.add = true), (this.count = false);
                        }
                    }
                },
            },
        })
    );
    new Vue({
        el: "#app",
        data: {
            add: true,
            count: false,
            total: false,
            totalAmt: 0,
            discount: 0,
            paidAmt: 0,
            orderList: [
                {
                    item: "Dragon Chicken",
                    amt: 297,
                    increment: 0,
                },
                {
                    item: "Chicken 65",
                    amt: 263,
                    increment: 0,
                },
                {
                    item: "Chicken Lollipop",
                    amt: 290,
                    increment: 0,
                },
                {
                    item: "Black Pepper Chicken ",
                    amt: 335,
                    increment: 0,
                },
            ],
        },
        methods: {
            showTotalAmt() {
                this.total = true;
                this.totalAmt = 0;
                for (i = 0; i < this.orderList.length; i++) {
                    this.totalAmt += this.orderList[i].amt * this.orderList[i].increment;
                    this.discount = (this.totalAmt * 5) / 100;
                    this.paidAmt = this.totalAmt - this.discount + 20;
                }
            },
        },
    });
}