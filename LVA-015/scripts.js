function manageCategory() {
    Vue.component('add-categories', Vue.extend({
        template: '#addCategories',
        props: ['listofcat'],
        data: function () {
            return {
                addValue: '',
                addRow: false,
                addcontainer: false,
            }
        },
        methods: {
            addList() {
                const obj = {}
                obj['id'] = (this.listofcat.length) + 1,
                    obj['name'] = this.addValue
                this.listofcat.push(obj);
                this.addValue = '';
            },
            cancelList() {
                this.addValue = '';
            }
        },
    }));
    Vue.component('edit-categories', Vue.extend({
        template: '#editCategories',
        props: ['listofcat', 'selecteditem', 'selectedkey'],
        data: function () {
            return {
                editinput: this.selecteditem,
                editcontainer: false
            }
        },
        methods: {
            editList() {
                this.listofcat[this.selectedkey].name = this.editinput;
                this.editinput = '';
            },
            cancelList() {
                this.editinput = '';
            }
        }
    }));
    Vue.component('delete-categories', Vue.extend({
        template: '#deleteCategories',
        props: ['listofcat', 'selecteditem', 'selectedkey'],
        data: function () {
            return {
                deleteinput: this.selecteditem,
                deletecontainer: false
            }
        },
        methods: {
            deleteList() {
                this.listofcat.splice(this.selectedkey, 1);
                this.selecteditem = '';
            },
            cancelList() {
                this.deleteinput = '';
            }
        }
    }));
    new Vue({
        el: "#manage_category",
        data: {
            editbtn: false,
            deletebtn: false,
            deletecontainer: false,
            addcontainer: false,
            editcontainer: false,
            selecteditem: '',
            selectedkey: '',
            listofcat: [],
        },
        methods: {
            showbtn(cat) {
                this.editbtn = true;
                this.deletebtn = true;
                this.selecteditem = Object.values(cat)[1];
                // this.selectedkey = Object.keys(cat)[1];
                this.selectedkey = this.listofcat.indexOf(cat);
                var x = this.selectedkey;
                // var x =( this.selectedkey);
                console.log(x);
            },
            openaddrow() {
                this.addcontainer = true;
                this.editcontainer = false;
                this.deletecontainer = false;
            },
            openeditrow() {
                this.editcontainer = true;
                this.addcontainer = false;
                this.deletecontainer = false;
            },
            opendeleterow() {
                this.deletecontainer = true;
                this.addcontainer = false;
                this.editcontainer = false;
            },
        }
    })
}