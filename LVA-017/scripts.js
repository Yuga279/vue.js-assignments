// function pomodoroTimer() {
//     new Vue({
//         el: '#app',
//         data: {
//             startTimer: true,
//             work: false,
//             cancel: false,
//             timer: null,
//             break: null,
//             remainingTime: 1497,
//         },
//         methods: {
//             start() {
//                 this.work = true;
//                 this.startTimer = false;
//                 this.cancel = false;
//                 this.timer = setInterval(() => this.increaseTimer(), 1000)
//             },
//             increaseTimer() {

//                 this.remainingTime++;
//                 if (this.remainingTime == 1500) {
//                     this.remainingTime = 0
//                     this.work = false;
//                     this.startTimer = false;
//                     this.cancel = true;

//                     clearInterval(this.timer);
//                     this.timer = null;
//                     clearInterval(this.remainingTime);
//                     this.remainingTime = 0;
//                 }

//             },

//             clearTimes() {
//                 this.work = false;
//                 this.startTimer = true;
//                 this.cancel = false;
//                 clearInterval(this.timer);
//                 this.timer = null;
//                 clearInterval(this.remainingTime);
//                 this.remainingTime = null;
//             },
//         },
//         // breakTiming() {
//         //     this.break = setInterval(() => this.increaseTimer(), 1000)
//         //     if(remainingTime==0){
//         //         this.remainingTime++;

//         //     }
//         // },
//         computed: {
//             minutes: function () {
//                 var minutes = Math.floor(this.remainingTime / 60);
//                 return minutes;
//             },
//             seconds: function () {
//                 var seconds = this.remainingTime - (this.minutes * 60);
//                 return seconds;
//             }
//         },

//     })
// }
function pomodoroTimer() {
  new Vue({
    el: "#app",
    data: {
      startTimer: true,
      work: false,
      timer: null,
      remainingTime: 0,
      breakTimer: false,

    },
    methods: {
      start() {
        this.work = true;
        this.startTimer = false;
        this.timer = setInterval(() => this.remainingTime++, 1000);
      },
      cancelTimer() {
        this.work = false;
        this.startTimer = true;
        this.remainingTime = 0;
        clearInterval(this.timer);
      },
    },
    computed: {
      seconds: function () {
        var seconds = this.remainingTime % 60;
        if (this.minutes == 25 & seconds == 0) {
          seconds = 0;
          this.remainingTime = 0;
          this.work = false
          this.breakTimer = true;
        }
        return seconds;
      },
      minutes: function () {
        var minutes = Math.floor(this.remainingTime / 60);
        if (minutes == 5 & this.breakTimer == true) {
          this.remainingTime = 0;
          this.startTimer = true;
          this.breakTimer = false;
        }
        return minutes;
      },
    },
  });
}
