function todolist() {
    new Vue({
        el: "#todolists",
        data: {
            inputTodo: '',
            todos: []

        },
        methods: {
            createTodo() {
                this.todos.push(this.inputTodo);
                this.inputTodo = "";
            },
            deleteTodo(todo) {
                this.todos.splice(this.todos.indexOf(todo), 1);

            },
        }
    })
}