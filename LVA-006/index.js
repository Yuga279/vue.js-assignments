function urlbreaker() {
    new Vue({
        el: '#Assignment6',
        data: {
            url: '',
            protocol: '',
            DomainName: '',
            SubDomain: '',
            PortNumber: '',
            path: '',
            Query: '',
            Anchor: ''
        },
        methods: {

            geturl() {

                var a = new URL(this.url).protocol;
                var b = new URL(this.url).hostname;
                var c = new URL(this.url).host;
                var d = new URL(this.url).port;
                var e = new URL(this.url).pathname;
                var f = new URL(this.url).search;
                var g = new URL(this.url).hash;

                if (a != null || a == '') {
                    this.protocol = a
                } else {
                    this.protocol = 'NA'
                }
                if (b != null || b == '') {
                    this.DomainName = b
                } else {

                    this.DomainName = 'NA'

                }
                if (c != null || c == '') {
                    this.SubDomain = c
                } else {
                    this.SubDomain = 'NA'
                }
                if (d != null || d == '') {
                    this.PortNumber = d
                } else {

                    this.PortNumber = 'NA'

                }
                if (e != null || e == '') {
                    this.path = e
                } else {
                    this.path = 'NA'
                }
                if (f != null || f == '') {
                    this.Query = f
                } else {

                    this.Query = 'NA'

                }
                if (g != null || g == '') {
                    this.Anchor = g
                } else {

                    this.Anchor = 'NA'

                }


            },
            clearurl() {
                this.url = '',
                    this.protocol = '',
                    this.DomainName = '',
                    this.SubDomain = '',
                    this.PortNumber = '',
                    this.path = '',
                    this.Query = '',
                    this.Anchor = ''
            },
            gotourl() {
                window.location = this.url
            }
        }

    })
}